/*
 * This file is part of eyeo's mlaf module (@eyeo/mlaf),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * @eyeo/mlaf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @eyeo/mlaf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/mlaf.  If not, see <http://www.gnu.org/licenses/>.
 */
// noinspection JSUnusedLocalSymbols

const WHITESPACE_PATTERN = /\s+/g;
const PUNCTUATION_PATTERN = /[\xA0!"\\'()*,-./:;?&[\]^_`{|}~“”]/g;
const RGB_PATTERN = /rgb\((\d+), (\d+), (\d+)\)/;
const RGBA_PATTERN = /rgba\((\d+), (\d+), (\d+), (\d+)\)/;
const ORIGIN_PATTERN = /(\d*?\.?\d*)px (\d*?\.?\d*)px/;
const PIXEL_PATTERN = /(\d*?\.?\d*)px/;
const DOT_PATTERN = /\./g;
const BRACKETS_PATTERN = /^\[|\]$/g;
const SLASH_PATTERN = /\//g;
const WORD_INDEX =
  Object.fromEntries([..." abcdefghijklmnopqrstuvwxyz1234567890:;/?!=+.,()[]-`*_|~"].map((c, i) => [c, i + 1]));


/**
 * Base class for normalizers.
 * Usage:
 *     from feature.normalizer import Lowercase
 *     from feature.normalizer import CollapseWhitespace
 *
 *     >>> f = FeatureNormalizer(Lowercase, CollapseWhitespace)
 *     >>> f.normalize(u'HouSEs   Var')
 *     >>> u'houses var'
 *
 *     Normalizers are applied in the order which they are passed in,
 *     so the outcome may differ only because of the ordering, e.g.:
 *
 *     >>> f1 = FeatureNormalizer(ReplacePunctuation, CollapseWhitespace)
 *     >>> f1.normalize('abc???abc')
 *     >>> 'abc abc'
 *     >>> f2 = FeatureNormalizer(CollapseWhitespace, ReplacePunctuation)
 *     >>> f2.normalize('abc???abc')
 *     >>> u'abc   abc'
 */
class FeatureNormalizer {
  constructor(args) {
    this.normalizers = [];
    args.forEach(normalizer => {
      if (Array.isArray(normalizer)) {
        let [norm, kwargs] = normalizer;
        this.normalizers.push(new norm(kwargs));
      }
      else {
        this.normalizers.push(new normalizer());
      }
    }
    );
  }

  normalize(feature, kwargs) {
    this.normalizers.forEach(normalizer => {
      feature = normalizer.normalize(feature, kwargs);
    });
    return feature;
  }
}


class StringNormalizer {
  normalize(str) {
    throw new Error("Not implemented.");
  }
}

/**
 *   Transform all characters to lowercase.
 *
 *     >>> normalize('AbC DeF')
 *     >>> 'abc def'
 */
class Lowercase extends StringNormalizer {
  normalize(str) {
    return str.toLowerCase();
  }
}

/**
 * Strip white spaces from the beginning and end of string.
 *
 *     >>> normalize('  a   ')
 *     >>> 'a'
 */
class StripWhitespace extends StringNormalizer {
  normalize(str) {
    return str.trim();
  }
}

/**
 * Replace multiple white spaces with a single whitespace.
 *
 *     >>> normalize('  a  b c d  ')
 *     >>> ' a b c d '
 */
class CollapseWhitespace extends StringNormalizer {
  normalize(str) {
    return str.replace(WHITESPACE_PATTERN, " ");
  }
}

/**
 * Strip and collapse white spaces from a string.
 */
class StripAndCollapseWhitespace extends StringNormalizer {
  normalize(str) {
    return str.replace(WHITESPACE_PATTERN, " ").trim();
  }
}

/**
 * Remove punctuation.
 */
class RemovePunctuation extends StringNormalizer {
  normalize(str) {
    return str.replace(PUNCTUATION_PATTERN, "");
  }
}

/**
 * Replace punctuation with a whitespace.
 */
class ReplacePunctuation extends StringNormalizer {
  normalize(str) {
    return str.replace(PUNCTUATION_PATTERN, " ");
  }
}

/**
 * Replace old with new values.
 */
class ReplaceStr extends StringNormalizer {
  constructor(pairs) {
    super();
    this.pairs = pairs;
  }

  normalize(str) {
    if (str) {
      this.pairs.forEach(pair => {
        str = str.split(pair[0]).join(pair[1]);
      });
    }
    return str;
  }
}

/**
 * Extract only the last CSS class in the CSS class sequence.
 * >>> normalize('rq0escxv l9j0dhe7 du4w35lb hybvsw6c io0zqebd m5lcvass')
 * >>> 'm5lcvass'
 */
class LastCSSClassNormalizer extends StringNormalizer {
  normalize(str) {
    return str.split(" ").pop();
  }
}


class VectorNormalizer {
  normalize(str) {
    throw new Error("Not implemented.");
  }
}

class FloatNormalizer extends VectorNormalizer {
  normalize(str) {
    return [parseFloat(str) || 0];
  }
}

/**
 * Generates a sequence for a given string based on a WORD_INDEX
 */
class SequenceNormalizer extends VectorNormalizer {
  /**
   * @param {number} maxLen - the maximum length of the sequences
   */
  constructor(maxLen) {
    super();
    this.maxLen = maxLen;
  }

  /**
   *
   * @param {string} str - the string to generate the sequence for
   * @returns {Array} - the generated sequence
   */
  normalize(str) {
    let seq = [...str.toLowerCase()].slice(-this.maxLen).map(c => c in WORD_INDEX ? WORD_INDEX[c] : 1);
    return seq.length >= this.maxLen ? seq : seq.concat(new Array(this.maxLen - seq.length).fill(0));
    // Candidates for potential better performance:
    // -------
    // return [...str.toLowerCase()]
    //   .slice(-this.maxLen)
    //   .map(c => WORD_INDEX[c] ?? 1)
    //   .concat(
    //     (new Array(Math.max(0, this.maxLen - str.length)).fill(0))
    //   );
    // -------
    // return Object.assign(
    //   (new Array(this.maxLen)).fill(0),
    //   [...str.toLowerCase()].slice(-this.maxLen).map(c => WORD_INDEX[c] ?? 1)
    // );
    // -------
  }
}

class OneHotVectorNormalizer extends VectorNormalizer {
  constructor(values) {
    super();
    const len = values.length;
    this.empty = new Array(len).fill(0);
    // Optimized for performance
    let i = 0;
    this.categoryMap = values.reduce((map, val) => {
      let oneHot = [...this.empty];
      oneHot[i++] = 1;
      map[val] = oneHot;
      return map;
    }, {});
  }

  normalize(str) {
    return this.categoryMap[str] || this.empty;
  }
}

class FontSizeNormalizer extends VectorNormalizer {
  normalize(str) {
    return [parseFloat(str) || 0];
  }
}

/**
 * Extract and normalize RGB colors from a CSS string.
 *
 *     >>> cn = ColorNormalizer(3)
 *     >>> cn.normalize('rgb(255, 0, 255);')
 *     >>> [255.0, 0.0, 255.0]
 *     >>> cn.normalize('rgba(255, 0, 255, 0);')
 *     >>> [255.0, 0.0, 255.0]
 *
 *     >>> cn = ColorNormalizer(4)
 *     >>> cn.normalize('rgb(255, 0, 255);')
 *     >>> [255.0, 0.0, 255.0, 0.0]
 *     >>> normalize('rgba(255, 0, 255, 0);')
 *     >>> [255.0, 0.0, 255.0, 0.0]
 *
 *     TODO: cover cases when color is not in RGB format, e.g. 'red', '#090a', or
 *     'hsl(30, 100%, 50%, 0.6)'
 *     How often do we see other color formats? -> 100% RGB
 */
class ColorNormalizer extends VectorNormalizer {
  /**
   * @param {number} maxLen - the maximum length of the sequences
   */
  constructor(maxLen) {
    super();
    this.maxLen = maxLen;
  }

  normalize(str) {
    let matchResult = [0.0, 0.0, 0.0, 0.0];
    const rgbMatch = str.match(RGB_PATTERN);
    if (rgbMatch) {
      rgbMatch.shift();
      matchResult = rgbMatch.map(v => parseFloat(v) || 0);
      matchResult.push(0.0);
    }
    else {
      const rgbaMatch = str.match(RGBA_PATTERN);
      if (rgbaMatch) {
        rgbaMatch.shift();
        matchResult = rgbaMatch.map(v => parseFloat(v) || 0);
      }
    }
    return matchResult.slice(0, this.maxLen);
  }
}

/**
 * Extract pixel sizes from a CSS string.
 *
 *     >>> normalize('100px;')
 *     >>> [100.0]
 *
 *     TODO: cover cases when the weight is not in pixels, e.g. 'auto', '20em',
 *     or '75%'
 *     How often do we see other weight formats? -> 86% px, 13% other
 *     TODO: normalize values from 0 to 1?
 */
class PixelNormalizer extends VectorNormalizer {
  normalize(str) {
    const match = str.match(PIXEL_PATTERN);
    if (match)
      return [parseFloat(match[1]) || 0];
    return [0.0];
  }
}

/**
 * Extract origins from a CSS string.
 *
 *     >>> normalize('100px 100px;')
 *     >>> [100.0, 100.0]
 *
 *     TODO: cover cases when origins are not in pixels, e.g. 'bottom right',
 *     '-170%', or '500% 200%'
 *     How often do we see other origin formats? -> 86% px, 13% other
 */
class OriginNormalizer extends VectorNormalizer {
  normalize(str) {
    const match = str.match(ORIGIN_PATTERN);
    if (match) {
      match.shift();
      return match.map(v => parseFloat(v) || 0);
    }
    return [0.0, 0.0];
  }
}

class NodeChildrenLenNormalizer {
  normalize(node) {
    if (Array.isArray(node))
      return [node.length];

    return [0];
  }
}

/**
 * Extract URL meta data.
 *
 *     Meta data is length and number of elements from URL domain, path and query.
 *
 *     return: [has_url, is_url, is3rd_party, host_len, path_len, qry_len,
 *              host_elems, path_elems, qry_elems]
 *
 *     >>> normalize('https://doubleclick.net', {'domain': 'www.facebook.com'})
 *     >>> [1, 1, 0, 15, 0, 0, 2, 0, 0]
 *     >>> normalize('https://doubleclick.net?a=1',
 *                   {'domain': 'www.facebook.com'})
 *     >>> [1, 1, 0, 15, 0, 3, 2, 0, 1]
 *     >>> normalize('https://doubleclick.net/pagead/ads',
 *                   {'domain': 'www.facebook.com'})
 *     >>> [1, 1, 0, 15, 11, 0, 2, 2, 0]
 *     >>> normalize('https://doubleclick.net/pagead/ads?a=1',
 *                   {'domain': 'www.facebook.com'})
 *     >>> [1, 1, 0, 15, 11, 3, 2, 2, 1]
 */
const URL_MARKERS = ["http", "data", "blob", "ftp:"];
class MetaURLPartsNormalizer {
  normalize(urlStr, kwargs) {
    if (!urlStr)
      return [0, 0, 0, 0, 0, 0, 0, 0, 0];
    const hasUrl = 1;
    const isUrl = URL_MARKERS.includes(urlStr.slice(0, 4).toLowerCase()) ? 1 : 0;
    if (!isUrl)
      return [hasUrl, isUrl, 0, 0, urlStr.length, 0, 0, 0, 0];
    let url;
    try {
      url = new URL(urlStr);
    }
    catch (e) {
      return [hasUrl, isUrl, 0, 0, urlStr.length, 0, 0, 0, 0];
    }
    const is3rdParty = url.hostname.includes(kwargs["domain"]) ? 0 : 1;
    const netloc = url.username + url.password + url.hostname + (url.port ? url.port : "");
    const path = url.pathname === "/" ? "" : url.pathname;
    const queryValues = Array.from(url.searchParams);

    const hostLen = netloc.length;
    const pathLen = path.length;
    const queryLen = queryValues.flat().join("").length;
    const hostParts = netloc ? url.hostname.split(".").length : 0;
    const pathParts = path.split("/").filter(Boolean).length;
    const queryParts = queryValues.length;

    return [hasUrl, isUrl, is3rdParty, hostLen, pathLen, queryLen, hostParts, pathParts, queryParts];
  }
}

/**
 *     Extract URL domain, path and query parameters (w/o their values).
 *
 *     >>> urln = URLPartsNormalizer(128)
 *     >>> urln.normalize('https://doubleclick.net')
 *     >>> 'doubleclick net'
 *     >>> urln.normalize('https://doubleclick.net/pagead/ads')
 *     >>> 'doubleclick net pagead ads'
 *     >>> urln.normalize('https://doubleclick.net?a=1')
 *     >>>'doubleclick net a'
 *     >>> urln.normalize('https://doubleclick.net/pagead/ads?a=1')
 *     >>> 'doubleclick net pagead ads'
 */
class URLPartsNormalizer {
  constructor(maxLen) {
    this.maxLen = maxLen;
    this.maxPartsLen = this.maxLen / 3;
    this.replacePunctuation = new ReplacePunctuation();
  }

  normalize(urlStr, kwargs) {
    let url;
    const isUrl = URL_MARKERS.includes(urlStr.slice(0, 4).toLowerCase()) ? 1 : 0;
    if (!isUrl)
      return urlStr.substring(0, this.maxLen).trim();
    try {
      url = new URL(urlStr);
    }
    catch (e) {
      return urlStr.substring(0, this.maxLen).trim();
    }

    const hostname = url.hostname.replace(BRACKETS_PATTERN, "");
    const host = [url.username, url.password, hostname.replace(DOT_PATTERN, " "), url.port]
      .filter(Boolean)
      .join(" ")
      .slice(0, this.maxPartsLen).trim();
    const path = (url.pathname.replace(SLASH_PATTERN, " ")).slice(0, 2 * this.maxPartsLen - host.length).trim();
    const queryPairs = url.search.substring(1).split("&")
      .map(a => a.split("=")[0].trim())
      .join(" ").slice(0, 3 * this.maxPartsLen - host.length - path.length).trim();
    return [[host, path].join(" ").trim(), queryPairs].join(" ").trim();
  }
}

class MetaContentNormalizer {
  normalize(str, kwargs) {
    let contentLen = 0;
    let contentParts = 0;

    if (str) {
      contentLen = str.length;
      contentParts = str.split(" ").length;
    }
    return [contentLen, contentParts];
  }
}

/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-env commonjs */

class FeaturePreProcessor {
  constructor(args) {
    this.preprocesors = [];
    for (let preprocesor of args)
      this.preprocesors.push(new preprocesor());
  }

  // noinspection JSUnusedLocalSymbols
  process(data, kwargs) {
    this.preprocesors.forEach(preprocessor =>
      data = preprocessor.process(data));
    return data;
  }
}

class EmptyPreProcessor {
  // noinspection JSUnusedLocalSymbols
  process(data, kwargs) {
    if (!data)
      return new Map();
    return data;
  }
}

class CSSSelectorPreProcessor {
  // noinspection JSUnusedLocalSymbols
  process(data, kwargs) {
    if (!data || !(typeof data === "string" || data instanceof String))
      return new Map();

    return data.split(";").reduce((map, pair) => {
      const [key, val] = pair.split(":");
      if (key && val && !(key in map))
        map[key.trim()] = val.trim();
      return map;
    }, {});
  }
}

/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Reads the JSON configuration and produces feature parsers
 * @param jsonConfig
 * @returns {Array<Array, Array>} featureGroups for nodes and graph
 */
function createFeatureGroups(jsonConfig) {
  let graphFeatureGroups = [];
  let nodeFeatureGroups = [];

  for (let node of jsonConfig)
    node.groupName = Object.keys(node)[2];

  for (let group of jsonConfig) {
    if (group.scope === "graph" && group.include)
      graphFeatureGroups.push(new FeatureGroup(group.groupName, group[group.groupName]));
    else if (group.scope === "node" && group.include)
      nodeFeatureGroups.push(new FeatureGroup(group.groupName, group[group.groupName]));
  }

  return [nodeFeatureGroups, graphFeatureGroups];
}

const Preprocessor2Class = {
  empty: EmptyPreProcessor,
  css_selector: CSSSelectorPreProcessor
};

const Normalizers2Class = {
  lower: Lowercase,
  strip_whitespace: StripWhitespace,
  collapse_whitespace: CollapseWhitespace,
  strip_and_collapse_whitespace: StripAndCollapseWhitespace,
  remove_punctuation: RemovePunctuation,
  replace_punctuation: ReplacePunctuation,
  sequence: SequenceNormalizer,
  one_hot_vector: OneHotVectorNormalizer,
  font_size: FontSizeNormalizer,
  color: ColorNormalizer,
  pixel: PixelNormalizer,
  origin: OriginNormalizer,
  children_len: NodeChildrenLenNormalizer,
  replace_str: ReplaceStr,
  meta_url: MetaURLPartsNormalizer,
  url_parts: URLPartsNormalizer,
  meta_content: MetaContentNormalizer,
  css_class: LastCSSClassNormalizer,
  float: FloatNormalizer
};

const splitWithEmpty = (str, sep) => {
  const a = str.split(sep);
  if (a[0] === "" && a.length === 1)
    return [];
  return a;
};

/**
 * Extracts the ML feature
 */
class Feature {
  constructor(name, conf) {
    this.name = name;
    this.dim = conf.dim;

    // Mutually exclusive feature names such as: 'src', 'href' and 'xlink:href' should be concatenated with '^',
    // e.g.: 'src^href^xlink:href'
    this.altNames = splitWithEmpty(name, "^");

    // JSON path in cases when feature data is nested inside of a larger JSON object, for instance:
    //
    //    {'attributes': {'src': { 'url: '<url>' }}}
    //
    // the 'url' is in 'attributes' feature group, but not directly accessible with attributes.get('url') b/c it is
    // nested in 'src'.
    this.path = [];
    if (conf.path)
      this.path = splitWithEmpty(conf.path, "^");

    // create feature normalizers
    let classes = [];
    if (!Object.prototype.hasOwnProperty.call(conf, "normalizers")) {
      classes.push(Normalizers2Class.empty);
    }
    else {
      for (const ncKey in conf.normalizers) {
        const nc = conf.normalizers[ncKey];
        if (typeof nc === "string" || nc instanceof String) {
          classes.push(Normalizers2Class[nc]);
        }
        else if (typeof nc === "object" && nc !== null) {
          const nName = Object.keys(nc)[0];
          classes.push([Normalizers2Class[nName], Object.values(nc[nName])[0]]);
        }
      }
    }
    this.normalizer = new FeatureNormalizer(classes);
  }

  getName(featureData) {
    if (this.altNames && Object.keys(featureData).length > 0) {
      for (let altName of this.altNames) {
        if (altName in featureData)
          return altName;
      }
    }
    return this.name;
  }

  extractValues(data, kwargs) {
    let featureData = data;
    // Search feature JSON path if it exists
    this.path.forEach(p => {
      if (p in featureData)
        featureData = featureData[p];
    });
    const featureName = this.getName(featureData);
    const featureString = featureName in featureData ? featureData[featureName] : "";
    const featureVec = this.normalizer.normalize(featureString || "", kwargs);
    if (featureVec.length !== this.dim)
      throw new Error("Feature dimensionality mismatch");

    return featureVec;
  }
}

class FeatureGroup {
  constructor(name, conf) {
    this.name = name;
    this.path = splitWithEmpty(conf.path, "/");
    this.prep = this.initPreprocessing(conf);
    this.features = this.initFeatures(conf.features);
  }

  initPreprocessing(conf) {
    let classes = [];
    if (!Object.prototype.hasOwnProperty.call(conf, "prep")) {
      classes.push(Preprocessor2Class.empty);
    }
    else {
      for (let pp of conf.prep)
        classes.push(Preprocessor2Class[pp]);
    }
    return new FeaturePreProcessor(classes);
  }

  initFeatures(featureConfList) {
    let features = [];
    for (const feature of featureConfList) {
      const name = Object.keys(feature)[0];
      const conf = feature[name];
      if (!Object.prototype.hasOwnProperty.call(conf, "names")) {
        features.push(new Feature(name, conf));
        continue;
      }
      for (const fName of conf.names)
        features.push(new Feature(fName, conf));
    }
    return features;
  }

  extractValues(node, kwargs) {
    let data = node;
    // Search feature JSON path if it exists
    this.path.forEach(p => {
      if (p in data)
        data = data[p];
    });

    data = this.prep.process(data, kwargs);

    if (
      typeof data !== "object" ||
      Array.isArray(data) ||
      data === null
    ) {
      throw new Error(
        `path ${this.path}, value ${data}, expected ${typeof {}}, got ${data}`
      );
    }
    let values = [];
    this.features.forEach(feature =>
      values.push(...feature.extractValues(data, kwargs)));
    return values;
  }
}

/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

const URL_DATA_ATTRS = ["src", "href", "xlink:href"];

const zeros = (w, h, v = 0) => Array.from(new Array(h), _ => Array(w).fill(v));
const get = (o, k, d) => k in o ? o[k] : d;

class Graph {
  constructor(kwargs) {
    this.numNodes = 0;
    this.root = kwargs["obj"]["graph"];
    this.domain = kwargs["obj"]["domain"];
    this.graphAttributes = {domain: kwargs["obj"]["domain"]};
    this.featureGroups = kwargs["nodeFeatureGroups"];
    this.graphFeatureGroups = kwargs["graphFeatureGroups"];
    this.cutoff = kwargs["cutoff"];
  }

  getData() {
    // 1st graph traversal collects number of nodes and other stats
    this.setNodeAttributes();
    // 2nd graph traversal creates edges and features
    let edges = zeros(this.numNodes, this.numNodes);
    let features = [];
    let graphFeatures = [];
    // DFS graph traversal
    let toVisit = [[this.root, {}]];
    let cutoffCount = this.cutoff;
    while (toVisit.length && --cutoffCount >= 0) {
      let children;
      let [node, parent] = toVisit.pop();
      this._updateFeatures(node, this.featureGroups, features);
      if (parent && Object.keys(parent).length > 0)
        this._updateEdges(node, parent, edges);

      [parent, children] = [node, node["children"]];
      for (let i = children.length - 1; i >= 0; i--)
        toVisit.push([children[i], parent]);
    }

    this._updateFeatures(this.graphAttributes, this.graphFeatureGroups, graphFeatures);
    return this._formatData(edges, features, graphFeatures);
  }

  setNodeAttributes() {
    // DFS graph traversal
    let toVisit = [[this.root, {}]];
    let cutoffCount = this.cutoff;
    while (toVisit.length && --cutoffCount >= 0) {
      let children;
      let [node, parent] = toVisit.pop();
      this._setNodeAttributes(node, parent);
      [parent, children] = [node, node["children"]];
      for (let i = children.length - 1; i >= 0; i--)
        toVisit.push([children[i], parent]);
    }
  }

  _setNodeAttributes(node, parent) {
    node["node_id"] = this.numNodes++;
    node["dom"] = this.domain;
    node["parent_id"] = get(parent, "node_id", 0);
    node["siblings"] = get(parent, "children", []).length;
    node["level"] = get(parent, "level", -1) + 1;

    let nodeAttributes = get(node, "attributes", {});
    let parentAttributes = get(parent, "attributes", {});
    URL_DATA_ATTRS.forEach(attr => {
      let levKey = `${attr}_level`;
      if (this._hasAttribute(attr, nodeAttributes)) {
        node["attributes"][levKey] = 0;
      }
      else if (this._hasAttribute(attr, parentAttributes)) {
        node["attributes"][attr] = parentAttributes[attr];
        node["attributes"][levKey] = parentAttributes[levKey] + 1;
        if ("requestType" in parent)
          node["requestType"] = parent["requestType"];
      }
    });
  }

  _hasAttribute(attribute, nodeAttributes) {
    return attribute in nodeAttributes && nodeAttributes[attribute];
  }

  _updateFeatures(node, featureGroups, features) {
    let values = [];
    featureGroups.forEach(fg =>
      values.push(...fg.extractValues(node, {domain: this.domain}).map(val => Math.round(val * 100) / 100 || 0)));
    features.push(values);
  }

  _updateEdges(child, parent, edges) {
    edges[parent["node_id"]][child["node_id"]] = 1;
    edges[child["node_id"]][parent["node_id"]] = 1;
  }

  _formatData(edges, features, graphFeatures) {
    if (edges.length > features.length)
      this._cutEdges(edges, features.length);

    if (this.graphFeatureGroups.length > 0)
      return [edges, features, graphFeatures];
    return [edges, features];
  }

  _cutEdges(edges, length) {
    edges = edges.slice(0, length);
    edges.forEach((edge, i, arr) => {
      arr[i] = edge.slice(0, length);
    });
  }
}

/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * An `ML` object implements machine learning based on
 * {@link https://www.tensorflow.org/js TensorFlow.js}.
 *
 * Currently these preprocessing functions are supported:
 *
 * - `cast` - cast tensor into a type provided as an argument
 * - `stack` - {@link https://js.tensorflow.org/api/latest/#stack tf.stack}
 * - `unstack` - {@link https://js.tensorflow.org/api/latest/#unstack tf.unstack}
 * - `localPooling` - run local pooling on each element in an array
 * - `pad` - {@link https://js.tensorflow.org/api/latest/#pad tf.pad}
 *
 * @example <caption>Simple usage</caption>
 * let tf = require("@tensorflow/tfjs");
 * let ml = new ML(tf);
 * // Assuming a pretrained model that predicts (y = 2x - 1)
 *
 * // See here: https://github.com/tensorflow/tfjs-examples/blob/master/getting-started/index.js
 * ml.modelURL = "https://example.com/model.json";
 * console.log(await ml.predict([{data: [[20]]}]));
 * // Outputs approximately 39
 *
 * @example <caption>Usage with preprocessing options</caption>
 * let tf = require("@tensorflow/tfjs");
 * let ml = new ML(tf);
 * // Assuming a graph classification model
 * ml.modelURL = "https://example.com/model.json";
 * let adjacencyMatrices = [[0, 1, 0],
 *                          [1, 0, 1],
 *                          [0, 1, 0]];
 * let numberOfFeatures = 100;
 * let graphSize = 10;
 * let preprocessAdjacency = [
 *   {funcName: "pad", args: graphSize}
 *   {funcName: "unstack"}
 *   {funcName: "localPooling"}
 *   {funcName: "stack"}
 * ];
 * let featureVectors = [[1], [2], [3]];
 * let preprocessFeatures = [
 *   {funcName: "cast", args: "int32"}
 *   {funcName: "pad", args: graphSize}
 *   {funcName: "oneHot", args: numberOfFeatures}
 *   {funcName: "cast", args: "float32"}
 * ];
 * console.log(await ml.predict([
 *   {data: adjacencyMatrices, preprocess: preprocessAdjacency},
 *   {data: featureVectors, preprocess: preprocessFeatures}
 * ]));
 */
class ML {
  constructor(tfjs) {
    this.tfjs = tfjs;
    this._preprocessingFunctions = new Map([
      ["cast", tfjs.cast],
      ["stack", tfjs.stack],
      ["unstack", tfjs.unstack],
      ["localPooling", this._localPoolingMap.bind(this)],
      ["padAdjacency", this._padTensorAdjacency.bind(this)],
      ["padFeatures", this._padTensorFeatures.bind(this)]
    ]);
  }

  /**
   * Pads adjacency matrix tensor in two dimensions with `0`s or according
   * to the padSize given.
   *
   * See {@link https://js.tensorflow.org/api/latest/#pad here} for details.
   * @param {tfjs.Tensor} tensor
   * @param {number} padSize
   * @returns {tfjs.Tensor}
   * @private
   *
   * @example
   * // returns [[[0, 1, 0, 0, 0],
   *              [1, 0, 1, 0, 0],
   *              [0, 1, 0, 0, 0],
   *              [0, 0, 0, 0, 0],
   *              [0, 0, 0, 0, 0]]]
   * _padTensorAdjacency([[[0, 1, 0],
   *                       [1, 0, 1],
   *                       [0, 1, 0]]]
   *                      5
   *                     );
   */
  _padTensorAdjacency(tensor, padSize) {
    return tensor.pad([
      [0, 0],
      [0, padSize - tensor.shape[1]],
      [0, padSize - tensor.shape[2]]
    ]);
  }

  /**
   * Runs `{@link ML#_localPooling}` on an array of tensors.
   * @param {Array.<tfjs.Tensor>} adjacencyMatrices
   * @returns {Array.<tfjs.Tensor>}
   * @private
   */
  _localPoolingMap(adjacencyMatrices) {
    return adjacencyMatrices.map(x => this._localPooling(x));
  }

  /**
   * Computes a pooling operation for a matrix `A: (D^(-0.5))*(A)*(D^(-0.5))`
   * where `D` is the diagonal node degree matrix.
   *
   * Based on Kipf & Welling (2017).
   *
   * See {@link https://github.com/danielegrattarola/spektral/blob/a3883117b16b958e2e24723afc6885fbc7df397a/spektral/utils/convolution.py#L88 Python implementation}
   * @param {tfjs.Tensor} adjacencyMatrix
   * @param {tfjs} TensorFlow.js instance
   * @returns {tfjs.Tensor} The normalized adjacency matrix.
   * @private
   */
  _localPooling(adjacencyMatrix) {
    let aTilde = adjacencyMatrix.add(this.tfjs.eye(adjacencyMatrix.shape[0]));
    let degreePow = this.tfjs.pow(this.tfjs.sum(aTilde, 1), -0.5);
    degreePow = degreePow.where(
      this.tfjs.isFinite(degreePow),
      this.tfjs.zeros(degreePow.shape)
    );
    let degreeMx = degreePow.mul(this.tfjs.eye(aTilde.shape[0]));
    let normalized = degreeMx.dot(aTilde).dot(degreeMx);
    return normalized;
  }


  /**
   * Pads feature matrix tensor in one dimension with `0`s according
   * to the padSize given.
   *
   * See {@link https://js.tensorflow.org/api/latest/#pad here} for details.
   * @param {tfjs.Tensor} tensor
   * @param {number} padSize
   * @returns {tfjs.Tensor}
   * @private
   *
   * @example
   * // returns [[[0, 0, 0],
   *              [1, 0, 0],
   *              [2, 1, 0],
   *              [0, 0, 0],
   *              [0, 0, 0]]]
   * _padTensorAdjacency([[[0, 0, 0],
   *                       [1, 0, 0],
   *                       [2, 1, 0]]]
   *                      5
   *                     );
   */
  _padTensorFeatures(tensor, padSize) {
    return tensor.pad([
      [0, 0],
      [0, padSize - tensor.shape[1]],
      [0, 0]
    ]);
  }


  createTensors(data, cutoff) {
    const [adjacencyMatrix, features] = data;

    const process = [
      {
        data: [adjacencyMatrix], preprocess: [
          {funcName: "padAdjacency", args: cutoff}
        ]
      },
      {
        data: [features], preprocess: [
          {funcName: "padFeatures", args: cutoff}
        ]
      }
    ];

    let tensors = this.tfjs.tidy(() => process.map(input => {
      let preprocessed = this.tfjs.tensor(input.data);
      if (input.preprocess) {
        for (let params of input.preprocess) {
          preprocessed = this._preprocessingFunctions.get(params.funcName)(
            preprocessed, params.args
          );
        }
      }
      return preprocessed;
    }));

    return tensors;
  }
}

/* globals atob getComputedStyle */

const DEFAULT_GRAPH_CUTOFF = 500;

/**
 * Run inference
 * @param {Object} tfjs - TensorFlow.js instance
 * @param {Object} model - TensorFlow.js model
 * @param {Object} config - Data preprocessing config as a JSON object
 * @param {Object} target - Dom element to turn into graph
 * @param {string} domain - Current domain name (window.location.hostname), e.g. "facebook.com"
 * @returns {Promise.<Boolean>} Returns a boolean based on inference results
 */
async function inference(tfjs, model, bundle, target, domain = ".") {
  return domToGraph(bundle, target)
    .then(graph => preprocessGraph(bundle, graph, domain))
    .then(preprocessedGraph => predict(tfjs, model, bundle, preprocessedGraph))
    .then(predictions => digestPrediction(predictions));
}

/**
 * Turn a dom element into a graph
 * @param {Object} bundle - Model bundle as a JSON object
 * @param {Object} target - Dom element to turn into graph
 * @returns {Promise.<Object>} Returns a JSON object representing target as a graph
 */
async function domToGraph(bundle, target) {
  return new Promise((resolve, reject) => {
    if (!bundle || !target)
      return reject();

    let config = bundle.config;
    let cutoff = config.cutoff || bundle.topology.graphml.nodes || DEFAULT_GRAPH_CUTOFF;

    // Pre-compute node names to speed up traversal
    config = config.filter(e => e.include);
    for (let node of config)
      node.groupName = Object.keys(node)[2];

    let _extract = (element, data, group, attribute) => {
      if (group === "attributes" && typeof element.attributes[attribute] !== "undefined")
        data.attributes[attribute] = element.attributes[attribute].value;
      else if (group === "style" && element.style[attribute])
        data.attributes.style[attribute] = element.style[attribute];
      else if (group === "css")
        data.cssSelectors = getComputedStyle(element).cssText || "";
    };

    let _traverse = element => {
      cutoff -= 1;
      if (cutoff < 0)
        return;

      let data = {
        tag: element.tagName,
        width: element.clientWidth,
        height: element.clientHeight,
        attributes: {
          style: {}
        },
        children: []
      };

      for (let node of config) {
        for (let feature of node[node.groupName].features) {
          for (let [featureId, featureContent] of Object.entries(feature)) {
            if ("names" in featureContent) {
              for (let featureName of featureContent.names) {
                for (let featureNameFragment of featureName.split("^"))
                  _extract(element, data, node.groupName, featureNameFragment);
              }
            }
            else {
              _extract(element, data, node.groupName, featureId);
            }
          }
        }
      }

      if (element.children) {
        for (let child of element.children) {
          let childGraph = _traverse(child);
          if (childGraph)
            data.children.push(childGraph);
        }
      }

      return data;
    };

    let graph = _traverse(target);
    resolve(graph);
  });
}

/**
 * Perform data preprocessing
 * @param {Object} bundle - Model bundle as a JSON object
 * @param {Object} graph - JSON object representing target as a graph
 * @returns {Promise.<Object>} Returns Object containing preprocessed data
 */
async function preprocessGraph(bundle, graph, domain = ".") {
  return new Promise((resolve, reject) => {
    if (!bundle || !graph)
      return reject();

    let config = bundle.config;
    let cutoff = config.cutoff || bundle.topology.graphml.nodes || DEFAULT_GRAPH_CUTOFF;

    let [nodeFeatureGroups, graphFeatureGroups] = createFeatureGroups(config);
    let kwargs = {nodeFeatureGroups, graphFeatureGroups, obj: {domain, graph}, cutoff};

    const g = new Graph(kwargs);
    const data = g.getData();

    resolve(data);
  });
}

/**
 * Runs inference
 * @param {Object} tfjs - TensorFlow.js instance
 * @param {Object} model - TensorFlow.js model
 * @param {Object} bundle - Model bundle as a JSON object
 * @param {Object} input - Object containing adjacencyMatrix and feature matrix
 * @returns {Promise.<Array>} Returns array of predictions
 */
async function predict(tfjs, model, bundle, input) {
  return new Promise((resolve, reject) => {
    if (!bundle || !tfjs || !model || !input)
      return reject();

    let config = bundle.config;
    let cutoff = config.cutoff || bundle.topology.graphml.nodes || DEFAULT_GRAPH_CUTOFF;
    let tensorUtils = new ML(tfjs);
    let tensors = tensorUtils.createTensors(input, cutoff);

    // Run preprocessed graph against TFJS
    let result = model.execute(tensors);
    result.data().then(data => {
      resolve(data);
    }).catch(err => {
      reject(err);
    }).finally(() => {
      result.dispose();
      for (let inputTensor of tensors)
        inputTensor.dispose();
    });
  });
}


/**
 * Normalizes predictions
 * @param {Object} predictions - An array of predictions
 * @returns {Promise.<Array>} Returns array of predictions
 */
async function digestPrediction(predictions) {
  return new Promise((resolve, reject) => {
    // Check if TFJS' predictions are true or false
    predictions = Array.from(predictions);
    if (!predictions || !Array.isArray(predictions) || !predictions.some(v => v > 0) || predictions.length < 2)
      return reject();
    let norm = predictions.reduce((acc, val) => acc + val, 0);
    predictions = predictions.map(val => val / norm);
    resolve(predictions[1] > predictions[0]);
  });
}

/**
 * Loads model from a model bundle.
 * @param {Object} tfjs - A TFJS instance
 * @param {Object} bundle - Model bundle as a JSON object
 * @returns {Promise.<Array.<number>>} A promise that fulfills with an
 *   instance of a model.
 */
async function loadBundledModel(tfjs, bundle) {
  if (!bundle)
    return Promise.reject("model not found");

  return tfjs.loadGraphModel({
    load() {
      let data = {
        modelTopology: bundle.topology.modelTopology || {},
        signature: bundle.topology.signature || {},
        weightsManifest: bundle.topology.weightsManifest || [],
        weightSpecs: bundle.topology.weightsManifest.map(e => e.weights)[0] || [],
        weightData: concatWeights(
          bundle.weights.map(d => Uint8Array.from(atob(d.shard), bin => bin.charCodeAt(0)))
        ) || []
      };
      return data;
    }
  });
}

/**
 * Loads model from URL.
 * @param {Object} tfjs - A TFJS instance
 * @param {String} url - A URL to a model
 * @returns {Promise.<Array.<number>>} A promise that fulfills with an
 *   instance of a model.
 */
async function loadModel(tfjs, url) {
  return tfjs.loadGraphModel(url);
}

/**
 * Concats weights.
 * @param {Uint8Array} buffers - A buffer of weights
 * @returns {Uint8Array} A buffer of concatenated weights
 *   instance of a model.
 */
function concatWeights(buffers) {
  let totalByteLength = 0;

  buffers.forEach(buffer => {
    totalByteLength += buffer.byteLength;
  });

  let concatBuffer = new Uint8Array(totalByteLength);
  let offset = 0;
  buffers.forEach(buffer => {
    concatBuffer.set(new Uint8Array(buffer), offset);
    offset += buffer.byteLength;
  });
  return concatBuffer.buffer;
}

export { digestPrediction, domToGraph, inference, loadBundledModel, loadModel, predict, preprocessGraph };
