import fs from "fs";
import {join, dirname} from "path";
import {fileURLToPath} from "url";
import {spawnSync} from "child_process";

const MLBROWSERUTILS_ORIGIN = "git@gitlab.com:eyeo/machine-learning/mlbrowserutils.git";
const MLBROWSERUTILS_TAG = "main";

const DIR_ROOT = join(dirname(fileURLToPath(import.meta.url)), "..");
const DIR_LIB = join(DIR_ROOT, "lib");
const DIR_MLBROWSERUTILS = join(DIR_ROOT, ".temp_mlbrowserutils");
const DIR_MLBROWSERUTILS_DIST = join(DIR_MLBROWSERUTILS, "dist");

const COL_CLEAR = "\u001b[0m";
const COL_CYAN = "\u001b[36m";
const COL_RED = "\u001b[31m";

printHeadline("Building @eyeo/mlaf");

// Check for build arguments: `npm run build -- <repo> <branch>`
// E.g. `npm run build ../mlbrowserutils/ feature_branch` will
// use a local feature branch to build mlaf instead of the default
// repository `MLBROWSERUTILS_ORIGIN #MLBROWSERUTILS_TAG`
let mlbrowserutilsOrigin = MLBROWSERUTILS_ORIGIN;
let mlbrowserutilsTag = MLBROWSERUTILS_TAG;
if (process.argv && process.argv.length >= 2 && process.argv[2]) {
  mlbrowserutilsOrigin = process.argv[2];
  // Turn into absolute path if provided string isn't a URL and
  // isn't already absolute
  if (!mlbrowserutilsOrigin.includes("://") &&
    mlbrowserutilsOrigin.slice(0, 1) !== "/" &&
    mlbrowserutilsOrigin.slice(1, 2) !== ":\\")
    mlbrowserutilsOrigin = join(DIR_ROOT, mlbrowserutilsOrigin);
}
if (process.argv && process.argv.length >= 3 && process.argv[3])
  mlbrowserutilsTag = process.argv[3];

// Clean up
printProgress("Cleaning up");
cleanUpTemp();
fs.mkdirSync(DIR_MLBROWSERUTILS);

// Clone & build mlbrowserutils
printProgress("Cloning mlbrowserutils");
exec("git", ["clone", mlbrowserutilsOrigin, "--branch", mlbrowserutilsTag, "--single-branch", "-c", "advice.detachedHead=false", DIR_MLBROWSERUTILS], DIR_MLBROWSERUTILS);
printProgress("Installing mlbrowserutils' dependencies");
exec("npm", ["i"], DIR_MLBROWSERUTILS);
printProgress("Building mlbrowserutils");
exec("npm", ["run", "build"], DIR_MLBROWSERUTILS);
printProgress("Testing mlbrowserutils");
exec("npm", ["run", "test"], DIR_MLBROWSERUTILS);

// Populate lib & run tests
printProgress("Copying mlbrowserutils dist");
fs.rmSync(DIR_LIB, {recursive: true, force: true});
fs.cpSync(DIR_MLBROWSERUTILS_DIST, DIR_LIB, {recursive: true});

// Cleanup
printProgress("Cleaning up");
cleanUpTemp();

function printHeadline(headline) {
  if (headline) {
    process.stdout.write(`${COL_CYAN}${"-".repeat(14 + headline.length)}\n`);
    process.stdout.write(`${COL_CYAN}=== ${COL_CLEAR}MLAF: ${headline} ${COL_CYAN}===\n`);
    process.stdout.write(`${COL_CYAN}${"-".repeat(14 + headline.length)}\n`);
  }
}

function printProgress(text) {
  if (text)
    process.stdout.write(`${COL_CYAN}* MLAF: ${COL_CLEAR}${text}\n`);
}

function exec(command, args, cwd) {
  if (command) {
    try {
      let ret = spawnSync(command, args, {shell: true, stdio: "inherit", cwd});
      if (ret.status !== 0)
        die("Error during subprocess");
    }
    catch (err) {
      die(err);
    }
  }
}

function cleanUpTemp() {
  fs.rmSync(DIR_MLBROWSERUTILS, {recursive: true, force: true});
}

function die(err) {
  if (err)
    process.stdout.write(`${COL_RED}ERROR:${COL_CLEAR} ${err}\n`);
  cleanUpTemp();
  process.exit(1);
}
